package com.sharath;

import static org.junit.Assert.assertTrue;

import com.sharath.exceptions.WrongAgeException;
import com.sharath.exceptions.WrongNameException;
import com.sharath.validator.AgeValidator;
import com.sharath.validator.DateFormatValidator;
import com.sharath.validator.NameLengthValidator;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:MyApplicationContext.xml"})
public class AppTestWithXmlConfiguration {

    @Test
    public void shouldAnswerWithTrue() { assertTrue( true ); }

    @Test
    public void isNameLengthValid() throws WrongNameException {
        Assert.assertTrue(NameLengthValidator.validateName("krish"));
        Assert.assertTrue(NameLengthValidator.validateName("s"));
        Assert.assertTrue(NameLengthValidator.validateName("s"));
        Assert.assertTrue(NameLengthValidator.validateName("sharath"));
        Assert.assertTrue(NameLengthValidator.validateName("s"));
        Assert.assertTrue(NameLengthValidator.validateName("chandra"));
    }

    @Test
    public void isDateFormatValid() {
        Assert.assertTrue(DateFormatValidator.validatingDate("2011-11-11"));
        Assert.assertTrue(DateFormatValidator.validatingDate("11-11-2011"));
    }


    @Test
    public void isAgeValid() throws WrongAgeException {
        AgeValidator.validateAge("2011-11-11");
        AgeValidator.validateAge("2001-11-11");
    }


}
