package com.sharath.main;

import com.sharath.exceptions.NoMoreSeatsAvailableException;
import com.sharath.exceptions.WrongAgeException;
import com.sharath.exceptions.WrongNameException;
import com.sharath.userend.CommandLineInputCollector;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import java.sql.SQLException;


public class SchoolAnnotationLauncher {

    public static void main(String[] args) throws ClassNotFoundException, SQLException, WrongAgeException, WrongNameException, NoMoreSeatsAvailableException {

        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext("com.sharath");
        CommandLineInputCollector cmdIpHelper = applicationContext.getBean("cmdIpHelper", CommandLineInputCollector.class);
        cmdIpHelper.process();
    }
}
