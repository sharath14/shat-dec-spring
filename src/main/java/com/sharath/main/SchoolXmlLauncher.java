package com.sharath.main;

import com.sharath.exceptions.NoMoreSeatsAvailableException;
import com.sharath.exceptions.WrongAgeException;
import com.sharath.exceptions.WrongNameException;
import com.sharath.userend.CommandLineInputCollector;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.sql.SQLException;

public class SchoolXmlLauncher {

    public static void main(String[] args) throws SQLException, ClassNotFoundException, WrongNameException, WrongAgeException, NoMoreSeatsAvailableException {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("MyApplicationContext.xml");
        CommandLineInputCollector cmdIpHelper = context.getBean("cmdIpHelper", CommandLineInputCollector.class);
        cmdIpHelper.process();
    }
}
