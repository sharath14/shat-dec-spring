package com.sharath.model;
import org.springframework.stereotype.Component;

@Component
public class StudentInfo {
    private StudentInfo studentInfo;
    private String studentName;
    private int studentAge;
    private GradeType studentGrade;
    private SchoolType schoolType;
    private Double schoolFees;


    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public int getStudentAge() {
        return studentAge;
    }

    public void setStudentAge(int studentAge) {
        this.studentAge = studentAge;
    }

    public GradeType getStudentGrade() {
        return studentGrade;
    }

    public void setStudentGrade(GradeType studentGrade) {
        this.studentGrade = studentGrade;
    }

    public SchoolType getSchoolType() {
        return schoolType;
    }

    public void setSchoolType(SchoolType schoolType) {
        this.schoolType = schoolType;
    }

    public Double getSchoolFees() {
        return schoolFees;
    }

    public void setSchoolFees(Double schoolFees) {
        this.schoolFees = schoolFees;
    }

    public StudentInfo() {
    }

    @Override
    public String toString() {
        return "StudentInfo{" +
                "studentName='" + studentName + '\'' +
                ", studentAge=" + studentAge +
                ", studentGrade=" + studentGrade +
                ", schoolType=" + schoolType +
                ", schoolFees=" + schoolFees +
                '}';
    }
}
