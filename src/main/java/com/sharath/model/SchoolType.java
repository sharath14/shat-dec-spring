package com.sharath.model;

public enum SchoolType {
    PRE_SCHOOL,
    ELEMENTARY_SCHOOL,
    MIDDLE_SCHOOL,
    HIGH_SCHOOL;
}
