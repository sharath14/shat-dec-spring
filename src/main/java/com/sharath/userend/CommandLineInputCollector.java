package com.sharath.userend;

import com.sharath.exceptions.NoMoreSeatsAvailableException;
import com.sharath.exceptions.WrongAgeException;
import com.sharath.exceptions.WrongNameException;
import com.sharath.model.GradeType;
import com.sharath.model.SchoolType;
import com.sharath.model.StudentInfo;
import com.sharath.services.ProcessAdmission;
import com.sharath.utility.Determinator;
import com.sharath.validator.DateFormatValidator;
import com.sharath.validator.AgeValidator;
import com.sharath.validator.NameLengthValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Scanner;
@Component("cmdIpHelper")
public class CommandLineInputCollector {
    static Logger logger = LoggerFactory.getLogger(CommandLineInputCollector.class);
    @Autowired
    private StudentInfo studentInfo;
    @Autowired
    private ProcessAdmission admissionProcess;

    public void setStudentInfo(StudentInfo studentInfo) {
        this.studentInfo = studentInfo;
    }

    public void setAdmissionProcess(ProcessAdmission admissionProcess) {
        this.admissionProcess = admissionProcess;
    }

    public void process() throws WrongNameException, WrongAgeException, SQLException, ClassNotFoundException, NoMoreSeatsAvailableException {
        String studentName;
        String doB;
        int age;
        do {
            Scanner scanner = new Scanner(System.in);
            logger.info("Enter First & Last name");
            studentName = scanner.nextLine();
            NameLengthValidator.validateName(studentName);
        } while (studentName.length() >= 50 || studentName.length() < 3);
        do {
            Scanner ipDate = new Scanner(System.in);
            logger.info("Enter the Date Of Birth in the following format: {}" ,LocalDate.now());
            doB = ipDate.next();
            DateFormatValidator.validatingDate(doB);
            age = AgeValidator.validateAge(doB);
        } while ((age > 17 || age < 5));
        studentInfo.setStudentName(studentName);
        studentInfo.setStudentAge(age);
        GradeType grade = Determinator.determineGrade(age);
        studentInfo.setStudentGrade(grade);
        SchoolType schoolType = Determinator.determineSchoolType(grade);
        studentInfo.setSchoolType(schoolType);
        Double fees = Determinator.determiningSchoolFees(grade);
        studentInfo.setSchoolFees(fees);
        System.out.println(studentInfo.toString());
        admissionProcess.admit(studentInfo);
    }
}
