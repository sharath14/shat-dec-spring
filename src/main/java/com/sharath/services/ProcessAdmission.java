package com.sharath.services;

import com.sharath.database.SchoolJDBCImpl;
import com.sharath.exceptions.NoMoreSeatsAvailableException;
import com.sharath.model.StudentInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.SQLException;

@Component
public class ProcessAdmission implements SchoolServices {
    @Autowired
    private SchoolJDBCImpl schoolJdbcImpl;

    public SchoolJDBCImpl getSchoolJdbcImpl() {
        return schoolJdbcImpl;
    }

    public void setSchoolJdbcImpl(SchoolJDBCImpl schoolJdbcImpl) {
        this.schoolJdbcImpl = schoolJdbcImpl;
    }

    @Override
    public void admit(StudentInfo studentInfo) throws SQLException, ClassNotFoundException, NoMoreSeatsAvailableException {
//        StudentInfo studentInfo1 = new StudentInfo();
//        String studentName = studentInfo1.getStudentName();
//        List<StudentInfo>studentInfoList=new ArrayList<>();
//        GradeType gradeType = studentInfo1.getStudentGrade();
//        if (schoolJdbcImpl.noOfStudentsInClass(studentName,gradeType)>= Max_Class_Size){
//            throw new NoMoreSeatsAvailableException("Maximum capacity of class is:  "+ Max_Class_Size +"No more seats available in class");
//        }
//            schoolJdbcImpl.saveStudent(studentInfo);
//            schoolJdbcImpl.retrieveStudentInfo(studentInfo);

//        Map<GradeType, List<StudentInfo>> getGradeTypeMapping;
//        System.out.println(getGradeTypeMapping.entrySet());
            schoolJdbcImpl.saveStudent(studentInfo);

    }
}
