package com.sharath.services;

import com.sharath.exceptions.NoMoreSeatsAvailableException;
import com.sharath.model.StudentInfo;

import java.sql.SQLException;

public interface SchoolServices {
    void admit(StudentInfo studentInfo) throws SQLException, ClassNotFoundException, NoMoreSeatsAvailableException;

}
