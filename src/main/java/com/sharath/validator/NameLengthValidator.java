package com.sharath.validator;

import com.sharath.exceptions.WrongNameException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NameLengthValidator {
    static Logger logger = LoggerFactory.getLogger(NameLengthValidator.class);
    public static boolean validateName(String sName) throws WrongNameException {
        if (sName.length() >= 50 || sName.length() < 3) {
            System.out.println("Student name is too small or long. Name should be no less than 3 characters or more than 50 characters");
        }
        return true;
    }
}
