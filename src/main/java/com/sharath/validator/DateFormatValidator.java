package com.sharath.validator;

import com.sharath.aspect.MethodAdvices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateFormatValidator {
    static Logger logger = LoggerFactory.getLogger(DateFormatValidator.class);
    public static boolean validatingDate(String doB) {
        if (!doB.trim().equals("")) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            simpleDateFormat.setLenient(false);

            Date parse = null;
            try {
                parse = simpleDateFormat.parse(doB);
            } catch (ParseException e) {
                logger.info( "Date format is not valid format");
                //System.exit(0);
            }
        }
        return true;
    }
}
