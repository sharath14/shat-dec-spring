package com.sharath.validator;

import com.sharath.exceptions.WrongAgeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.time.Period;

public class AgeValidator {

    static Logger logger = LoggerFactory.getLogger(DateFormatValidator.class);
    public static int validateAge(String doB) throws WrongAgeException {
        int sAge;
        do {
            LocalDate today = LocalDate.now();
            Period p = Period.between(LocalDate.parse(doB), today);
            sAge = p.getYears();
            if (sAge > 17 || sAge < 5) {
                logger.info("Age is too low or high. To enroll student should be at least 6 years old and not older than 17 years");
            }
            return sAge;

        } while ((sAge > 17 || sAge < 5));
    }
}
