package com.sharath.exceptions;

public class NoMoreSeatsAvailableException extends Exception {
    public NoMoreSeatsAvailableException(String message) {
        super(message);
    }
}
