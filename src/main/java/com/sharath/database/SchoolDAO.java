package com.sharath.database;

import com.sharath.model.GradeType;
import com.sharath.model.SchoolType;
import com.sharath.model.StudentInfo;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public interface SchoolDAO {

    void saveStudent(StudentInfo studentInfo);



    Integer noOfStudentsInClass(String studentName, GradeType gradeType);

}
