package com.sharath.database;

import com.sharath.model.GradeType;
import com.sharath.model.StudentInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;
import java.sql.PreparedStatement;

@Component
public class SchoolJDBCImpl implements SchoolDAO {
    static Logger logger = LoggerFactory.getLogger(SchoolJDBCImpl.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }


    @Override
    public void saveStudent(StudentInfo studentInfo) {

        String saveStudentQuery =
                "insert into studentinfo (name,age, st_g_id) values (?,?," +
                        " (SELECT g.grade_id from grade g" +
                        "        WHERE g.grade_name=?) )";
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(
                connection ->{
                    PreparedStatement preparedStatement = connection.prepareStatement(saveStudentQuery, new String[]{"student_id"});
                    preparedStatement.setString(1,studentInfo.getStudentName());
                    preparedStatement.setInt(2,studentInfo.getStudentAge());
                    preparedStatement.setString(3, String.valueOf(studentInfo.getStudentGrade()));
                    return preparedStatement;
                },keyHolder);
        Number key = keyHolder.getKey();
        System.out.println("newly admitted student: "+ studentInfo.getStudentName() + " with student_id: " + key.longValue());
    }


    @Override
    public Integer noOfStudentsInClass(String studentName, GradeType gradeType) {
        return null;
    }



}
