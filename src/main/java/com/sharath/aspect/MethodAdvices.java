package com.sharath.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class MethodAdvices {
    static Logger logger = LoggerFactory.getLogger(MethodAdvices.class);
    @Before("execution(* com.sharath.userend.CommandLineInputCollector.process(..))")
    public void doAspectCheck(JoinPoint joinPoint) {
        logger.info("Doing Aspect check before Process");
    }
//    @After("execution(* com.sharath.userend.CommandLineInputCollector.process(..))")
//    public void statusCheck(JoinPoint joinPoint) {
//        logger.info("Completed the Aspect check successfully");
//    }
//
//    @Around("execution(* com.sharath.userend.CommandLineInputCollector.process(..))")
//    public void executionStatus(JoinPoint joinPoint) {
//        logger.info("Executing");
//    }
//
//    @Before("execution(* com.sharath.validator.NameLengthValidator.validateName(..)")
//    public void startValidating(JoinPoint joinPoint) {
//        logger.info("validating length of name");
//    }
}
