package com.sharath.utility;

import com.sharath.exceptions.ClassNotAvailableException;
import com.sharath.model.GradeType;
import com.sharath.model.SchoolType;

public class Determinator {
    public static GradeType determineGrade(int studentAge) {
        switch (studentAge) {
            case 5:
                return GradeType.JK;
            case 6:
                return GradeType.SK;
            case 7:
                return GradeType.Grade1;
            case 8:
                return GradeType.Grade2;
            case 9:
                return GradeType.Grade3;
            case 10:
                return GradeType.Grade4;
            case 11:
                return GradeType.Grade5;
            case 12:
                return GradeType.Grade6;
            case 13:
                return GradeType.Grade7;
            case 14:
                return GradeType.Grade8;
            case 15:
                return GradeType.Grade9;
            case 16:
                return GradeType.Grade10;
            default:
                throw new IllegalArgumentException("Unable to determine grade");
        }

    }

    public static SchoolType determineSchoolType(GradeType grade) {
        switch (grade) {
            case JK:
            case SK:
                return SchoolType.PRE_SCHOOL;
            case Grade1:
            case Grade2:
            case Grade3:
            case Grade4:
            case Grade5:
                return SchoolType.ELEMENTARY_SCHOOL;
            case Grade6:
            case Grade7:
            case Grade8:
                return SchoolType.MIDDLE_SCHOOL;
            case Grade9:
            case Grade10:
                return SchoolType.HIGH_SCHOOL;
        }
        try {
            throw new ClassNotAvailableException("No School Available");
        } catch (ClassNotAvailableException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Double determiningSchoolFees(GradeType grade) {
        Double feesJK = 100 * 1.1;
        Double feesSK = 0.25 * (100 * 1.1);
        Double feesG1 = 100 * 1.5;
        Double feesG2 = 0.3 * (100 * 1.5);
        Double feesG3 = 0.3 * (100 * 1.5);
        Double feesG4 = 0.3 * (100 * 1.5);
        Double feesG5 = 0.3 * (100 * 1.5);
        Double feesG6 = 0.3 * (100 * 1.5);
        Double feesG7 = 0.3 * (100 * 1.5);
        Double feesG8 = 0.3 * (100 * 1.5);
        Double feesG9 = 0.3 * (100 * 1.5);
        Double feesG10 = 0.3 * (100 * 1.5);
        switch (grade) {
            case JK:
                return feesJK;
            case SK:
                return feesSK;
            case Grade1:
                return feesG1;
            case Grade2:
                return feesG2;
            case Grade3:
                return feesG3;
            case Grade4:
                return feesG4;
            case Grade5:
                return feesG5;
            case Grade6:
                return feesG6;
            case Grade7:
                return feesG7;
            case Grade8:
                return feesG8;
            case Grade9:
                return feesG9;
            case Grade10:
                return feesG10;
        }
        return null;
    }
}
